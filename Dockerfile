#Dockerfile
#FROM python:3.7-slim
FROM python:3.8-slim-buster
RUN pip install flask
WORKDIR /app
COPY app.py /app/app.py
ENTRYPOINT ["python"]
CMD ["/app/app.py"]
